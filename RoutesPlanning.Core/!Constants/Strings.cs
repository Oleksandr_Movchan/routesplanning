namespace RoutesPlanning.Core
{
    public static class Strings
    {
        public const string Distance = "Total distance: {0:0.##} km";

        public const string Time = "Duration: {0:0.##} min";

        public const string FieldValidationRegex = "^([A-Za-z\\s]*)$";

        public const string ValidationErrorTitle = "Validation error!";

        public const string ValidationErrorBody = "Field should contain only latin letter and whitespaces and contain at least one character";

        public const string ValidationErrorOk = "Ok";
    }
}
