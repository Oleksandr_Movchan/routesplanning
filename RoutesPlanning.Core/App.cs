using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.ViewModels;
using RoutesPlanning.Infrastructure.Connectivity;
using RoutesPlanning.Services.ApiServices;
using RoutesPlanning.Services.ApiServices.Navigation;
using RoutesPlanning.Services.ApiServices.Weather;

namespace RoutesPlanning.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IConnectivityService,ConnectivityService>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IRequestService,RequestService>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<INavigationService,NavigationService>();
            Mvx.IoCProvider.LazyConstructAndRegisterSingleton<IWeatherService,WeatherService>();

            RegisterCustomAppStart<AppStart>();
        }
    }
}
