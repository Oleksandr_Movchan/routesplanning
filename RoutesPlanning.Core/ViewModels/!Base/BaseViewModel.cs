using MvvmCross.ViewModels;

namespace RoutesPlanning.Core.ViewModels
{
    public abstract class BaseViewModel : MvxViewModel
    {
    }

    public abstract class BaseViewModel<TParameters> : MvxViewModel<TParameters>
        where TParameters : class
    {
    }
}
