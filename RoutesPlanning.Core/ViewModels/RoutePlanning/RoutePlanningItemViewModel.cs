using MvvmCross.ViewModels;
using RoutesPlanning.Services.Models;

namespace RoutesPlanning.Core.ViewModels.RoutePlanning
{
    public class RoutePlanningItemViewModel : MvxViewModel
    {
        public RoutePlanningItemViewModel(Step step, string weather, double temperature)
        {
            Direction = step.Direction;
            Latitude = $"{step.Location.Lat:0.00}";
            Longitude = $"{step.Location.Lng:0.00}";
            Weather = weather;
            Temperature = $"{temperature}°C";
        }

        public string Direction { get; }

        public string Latitude { get; }

        public string Longitude { get; }

        public string Weather { get; }

        public string Temperature { get; }
    }
}
