using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using RoutesPlanning.Core.Parameters;
using RoutesPlanning.Services.ApiServices.Navigation;
using RoutesPlanning.Services.ApiServices.Weather;
using RoutesPlanning.Services.Models;

namespace RoutesPlanning.Core.ViewModels.RoutePlanning
{
    public class RoutePlanningViewModel : BaseViewModel<RoutePlanningParameters>
    {
        private readonly INavigationService _navigationService;
        private readonly IWeatherService _weatherService;
        private readonly IMvxNavigationService _mvxNavigationService;

        private string _from;
        private string _to;

        public RoutePlanningViewModel(
            INavigationService navigationService,
            IWeatherService weatherService,
            IMvxNavigationService mvxNavigationService)
        {
            _navigationService = navigationService;
            _weatherService = weatherService;
            _mvxNavigationService = mvxNavigationService;

            BackCommand = new MvxAsyncCommand(NavigateBack);
        }

        public bool IsBusy { get; set; }

        public bool IsEmpty { get; set; }

        public string Distance { get; set; }

        public string Time { get; set; }

        public MvxObservableCollection<RoutePlanningItemViewModel> Items { get; } = new MvxObservableCollection<RoutePlanningItemViewModel>();

        public IMvxAsyncCommand BackCommand { get; set; }

        public override void Prepare(RoutePlanningParameters parameter)
        {
            _from = parameter.From;
            _to = parameter.To;
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            IsBusy = true;

            var navigation = await LoadRouteNavigation();

            if (navigation == null || navigation.Distance == 0)
            {
                IsEmpty = true;
                IsBusy = false;
                return;
            }

            Distance = string.Format(Strings.Distance, navigation.Distance/1000);
            Time = string.Format(Strings.Time, navigation.Duration/60);

            await InitializeList(navigation);

            IsBusy = false;
        }

        private async Task<Navigation> LoadRouteNavigation()
        {
            try
            {
                var navigation = await _navigationService.GetNavigationAsync(_from, _to);
                return navigation;
            }
            catch (Exception e)
            {
                // Log
                return null;
            }
        }

        private async Task InitializeList(Navigation navigation)
        {
            try
            {
                var weatherTasks = new List<Task<Weather>>();
                navigation.Steps
                    .Select(x => x.Location)
                    .ToList()
                    .ForEach(l => weatherTasks.Add(_weatherService.GetWeatherAsync(l.Lat, l.Lng)));

                await Task.WhenAll(weatherTasks);

                var itemsToAdd = weatherTasks
                    .Select((t, i) => new RoutePlanningItemViewModel(navigation.Steps[i], t.Result.Description, t.Result.Temperature))
                    .ToList();

                Items.AddRange(itemsToAdd);
            }
            catch (Exception e)
            {
                // Log
            }
        }

        private async Task NavigateBack()
        {
            await _mvxNavigationService.Close(this);
        }
    }
}
