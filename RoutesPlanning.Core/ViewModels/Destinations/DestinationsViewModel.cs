using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using RoutesPlanning.Core.Parameters;
using RoutesPlanning.Core.ViewModels.RoutePlanning;
using RoutesPlanning.Infrastructure.Dialogs;

namespace RoutesPlanning.Core.ViewModels.Destinations
{
    public class DestinationsViewModel : BaseViewModel
    {
        private readonly IMvxNavigationService _mvxNavigationService;
        private readonly IDialogsService _dialogsService;

        public DestinationsViewModel(
            IMvxNavigationService mvxNavigationService,
            IDialogsService dialogsService)
        {
            _mvxNavigationService = mvxNavigationService;
            _dialogsService = dialogsService;

            CalculateNavigationCommand = new MvxAsyncCommand(CalculateNavigation);
        }

        public string From { get; set; }

        public string To { get; set; }

        public IMvxAsyncCommand CalculateNavigationCommand { get; }

        public override async Task Initialize()
        {
            await base.Initialize();
        }

        private async Task CalculateNavigation()
        {
            var regex = new Regex(Strings.FieldValidationRegex);
            if (!string.IsNullOrWhiteSpace(From) &&
                !string.IsNullOrWhiteSpace(To) &&
                regex.IsMatch(From) &&
                regex.IsMatch(To))
            {
                await NavigateToRoutePlanning();
            }
            else
            {
                await _dialogsService.ShowConfirmationDialog(Strings.ValidationErrorTitle, Strings.ValidationErrorBody, Strings.ValidationErrorOk);
            }

        }

        private async Task NavigateToRoutePlanning()
        {
            await _mvxNavigationService.Navigate<RoutePlanningViewModel, RoutePlanningParameters>(new RoutePlanningParameters(From, To));
        }
    }
}
