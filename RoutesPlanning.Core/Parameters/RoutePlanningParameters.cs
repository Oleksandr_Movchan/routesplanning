namespace RoutesPlanning.Core.Parameters
{
    public class RoutePlanningParameters
    {
        public RoutePlanningParameters(string from, string to)
        {
            From = from;
            To = to;
        }

        public string From { get; }

        public string To { get; }
    }
}
