using System.Threading.Tasks;

namespace RoutesPlanning.Infrastructure.Dialogs
{
    public interface IDialogsService
    {
        Task ShowConfirmationDialog(string title, string message, string okText);
    }
}
