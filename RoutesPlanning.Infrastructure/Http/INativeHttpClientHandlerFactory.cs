using System.Net.Http;

namespace RoutesPlanning.Infrastructure.Http
{
    public interface INativeHttpClientHandlerFactory
    {
        HttpMessageHandler Create();
    }
}
