using Xamarin.Essentials;

namespace RoutesPlanning.Infrastructure.Connectivity
{
    public class ConnectivityService : IConnectivityService
    {
        public bool IsConnected => Xamarin.Essentials.Connectivity.NetworkAccess != NetworkAccess.None;
    }
}
