namespace RoutesPlanning.Infrastructure.Connectivity
{
    public interface IConnectivityService
    {
        bool IsConnected { get; }
    }
}
