using Newtonsoft.Json;

namespace RoutesPlanning.Services.Models
{
    public record Navigation
    {
        [JsonProperty("duration")]
        public long Duration { get; set; }

        [JsonProperty("distance")]
        public long Distance { get; set; }

        [JsonProperty("steps")]
        public Step[] Steps { get; set; }
    }
}
