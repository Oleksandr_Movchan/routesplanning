using Newtonsoft.Json;

namespace RoutesPlanning.Services.Models
{
    public record Weather
    {
        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("temperature")]
        public long Temperature { get; set; }
    }
}
