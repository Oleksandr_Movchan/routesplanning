using Newtonsoft.Json;

namespace RoutesPlanning.Services.Models
{
    public record Step
    {
        [JsonProperty("direction")]
        public string Direction { get; set; }

        [JsonProperty("location")]
        public Location Location { get; set; }
    }
}