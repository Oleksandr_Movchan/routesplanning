using Newtonsoft.Json;

namespace RoutesPlanning.Services.Models
{
    public record Location
    {
        [JsonProperty("lat")]
        public double Lat { get; set; }

        [JsonProperty("lng")]
        public double Lng { get; set; }
    }
}