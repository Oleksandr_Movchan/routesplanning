using System.Threading.Tasks;

namespace RoutesPlanning.Services.ApiServices
{
    public interface IRequestService
    {
        Task<TResult> GetAsync<TResult>(string uri);
    }
}
