using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RoutesPlanning.Infrastructure.Connectivity;
using RoutesPlanning.Infrastructure.Http;

namespace RoutesPlanning.Services.ApiServices
{
    public class RequestService : IRequestService
    {
        private readonly IConnectivityService _connectivityService;
        private readonly INativeHttpClientHandlerFactory _httpClientHandlerFactory;

        public RequestService(
            IConnectivityService connectivityService,
            INativeHttpClientHandlerFactory httpClientHandlerFactory)
        {
            _connectivityService = connectivityService;
            _httpClientHandlerFactory = httpClientHandlerFactory;
        }

        public async Task<TResult> GetAsync<TResult>(string uri)
        {
            if (!_connectivityService.IsConnected)
            {
                return default;
            }

            using var httpClient = CreateHttpClientAsync();

            var response = await httpClient.GetAsync(uri);

            if (!response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();

#if DEBUG
                System.Diagnostics.Debug.WriteLine("======== HandleResponse ========");
                System.Diagnostics.Debug.WriteLine(content);
#endif

                throw new HttpRequestException();
            }

            var responseData = await response.Content.ReadAsStringAsync();

            return await Task.Run(() => JsonConvert.DeserializeObject<TResult>(responseData));
        }

        private HttpClient CreateHttpClientAsync()
        {
            var httpClient = new HttpClient(_httpClientHandlerFactory.Create());

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return httpClient;
        }
    }
}
