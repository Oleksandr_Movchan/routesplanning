using System;
using System.Text;
using System.Threading.Tasks;

namespace RoutesPlanning.Services.ApiServices.Navigation
{
    public class NavigationService : INavigationService
    {
        private readonly IRequestService _requestService;

        public NavigationService(IRequestService requestService)
        {
            _requestService = requestService;
        }

        public async Task<Models.Navigation> GetNavigationAsync(string from, string to)
        {
            try
            {
                var url = new StringBuilder(ApiConstants.RootApiUrl);
                url.AppendFormat(ApiConstants.Route, from, to);

                var response = await _requestService.GetAsync<Models.Navigation>(url.ToString());
                return response;
            }
            catch (Exception e)
            {
                // Log
            }

            return null;
        }
    }
}
