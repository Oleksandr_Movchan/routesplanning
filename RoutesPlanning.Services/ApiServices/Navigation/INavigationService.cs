using System.Threading.Tasks;

namespace RoutesPlanning.Services.ApiServices.Navigation
{
    public interface INavigationService
    {
        Task<Models.Navigation> GetNavigationAsync(string from, string to);
    }
}
