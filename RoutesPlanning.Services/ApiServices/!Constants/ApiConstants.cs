namespace RoutesPlanning.Services.ApiServices
{
    public static class ApiConstants
    {
        public const string RootApiUrl = "https://scrmobiletest.azurewebsites.net/api/";

        public const string Route = "route/{0}/{1}";

        public const string Weather = "Weather/{0}/{1}";
    }
}
