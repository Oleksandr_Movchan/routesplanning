using System.Threading.Tasks;

namespace RoutesPlanning.Services.ApiServices.Weather
{
    public interface IWeatherService
    {
        Task<Models.Weather> GetWeatherAsync(double latitude, double longitude);
    }
}
