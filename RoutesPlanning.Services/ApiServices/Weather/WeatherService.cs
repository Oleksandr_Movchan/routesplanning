using System;
using System.Text;
using System.Threading.Tasks;

namespace RoutesPlanning.Services.ApiServices.Weather
{
    public class WeatherService : IWeatherService
    {
        private readonly IRequestService _requestService;

        public WeatherService(IRequestService requestService)
        {
            _requestService = requestService;
        }

        public async Task<Models.Weather> GetWeatherAsync(double latitude, double longitude)
        {
            try
            {
                var url = new StringBuilder(ApiConstants.RootApiUrl);
                url.AppendFormat(ApiConstants.Weather, latitude, longitude);

                var response = await _requestService.GetAsync<Models.Weather>(url.ToString());
                return response;
            }
            catch (Exception)
            {
                // Log
            }

            return null;
        }
    }
}
