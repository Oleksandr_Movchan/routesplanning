using Android.Content.Res;
using Android.Graphics;
using Android.Util;

namespace RoutesPlanning.Android
{
    public static class ThemeExtensions
    {
        public static Color GetPrimaryColor(this Resources.Theme theme)
        {
            var value = new TypedValue();
            theme.ResolveAttribute(Resource.Attribute.primaryColor, value, true);

            return new Color(value.Data);
        }
    }
}
