using Android.App;
using AndroidX.AppCompat.App;
using AndroidX.Core.Content;

namespace RoutesPlanning.Android
{
    public static class ActivityExtensions
    {
        public static void AdjustToolbar(this Activity activity, int toolbarId, bool allowBackNavigation = true)
        {
            if (activity is AppCompatActivity appCompatActivity)
            {
                var toolbar = activity.FindViewById<AndroidX.AppCompat.Widget.Toolbar>(toolbarId);

                appCompatActivity.SetSupportActionBar(toolbar);
                appCompatActivity.SupportActionBar.SetDisplayHomeAsUpEnabled(true);

                if (allowBackNavigation)
                {
                    appCompatActivity.SupportActionBar.SetHomeAsUpIndicator(ContextCompat.GetDrawable(activity, Resource.Drawable.ic_back_24));
                }
            }
        }
    }
}
