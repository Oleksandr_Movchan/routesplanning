using System;
using System.Collections.Generic;
using MvvmCross.DroidX.RecyclerView.ItemTemplates;
using RoutesPlanning.Core.ViewModels.RoutePlanning;

namespace RoutesPlanning.Android.TemplateSelectors
{
    public class ItemsTemplateSelector : IMvxTemplateSelector
    {
        private readonly Dictionary<Type, int> _itemsTypeDictionary = new Dictionary<Type, int>
        {
            [typeof(RoutePlanningItemViewModel)] = Resource.Layout.holder_step_item
        };

        public int GetItemViewType(object forItemObject)
        {
            return _itemsTypeDictionary[forItemObject.GetType()];
        }

        public int GetItemLayoutId(int fromViewType)
        {
            return fromViewType;
        }

        public int ItemTemplateId { get; set; }
    }
}
