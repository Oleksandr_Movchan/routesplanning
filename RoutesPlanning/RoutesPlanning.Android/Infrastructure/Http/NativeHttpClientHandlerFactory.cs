using System.Net.Http;
using RoutesPlanning.Infrastructure.Http;
using Xamarin.Android.Net;

namespace RoutesPlanning.Android.Infrastructure.Http
{
    public class NativeHttpClientHandlerFactory : INativeHttpClientHandlerFactory
    {
        public HttpMessageHandler Create()
        {
            var nativeClientHandler = global::Android.OS.Build.VERSION.SdkInt >= global::Android.OS.BuildVersionCodes.Lollipop
                ? new AndroidClientHandler()
                : new HttpClientHandler();

            return nativeClientHandler;
        }
    }
}
