using System.Threading.Tasks;
using AndroidX.Fragment.App;
using Plugin.CurrentActivity;
using RoutesPlanning.Android.Views.Dialog;
using RoutesPlanning.Infrastructure.Dialogs;

namespace RoutesPlanning.Android.Infrastructure.Dialogs
{
    public class DialogsService : IDialogsService
    {
        private const string DialogFragmentTag = "UserDialog";

        public Task ShowConfirmationDialog(string title, string message, string okText)
        {
            var fragmentManager = (CrossCurrentActivity.Current.Activity as FragmentActivity)?.SupportFragmentManager;
            var alertDialogFragment = AlertDialogFragment.NewInstance(title, message, okText);

            return alertDialogFragment.ShowAsync(fragmentManager, DialogFragmentTag);
        }
    }
}
