using Android.App;
using Android.Content.PM;
using MvvmCross.Platforms.Android.Views;

namespace RoutesPlanning.Android.Views
{
    [Activity(
        MainLauncher = true,
        Theme = "@style/RoutesPlanningLightTheme",
        NoHistory = true,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity
    {
        public SplashScreen()
            : base(Resource.Layout.splash_screen)
        {
        }
    }
}
