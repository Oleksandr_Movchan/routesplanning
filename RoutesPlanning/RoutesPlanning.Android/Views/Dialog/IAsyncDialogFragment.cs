using System.Threading.Tasks;
using AndroidX.Fragment.App;

namespace RoutesPlanning.Android.Views.Dialog
{
    public interface IAsyncDialogFragment
    {
        Task<bool> ShowAsync(FragmentManager manager, string tag);
    }
}
