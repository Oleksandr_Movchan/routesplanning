using Android.App;
using Android.OS;
using Android.Views;
using MvvmCross.Platforms.Android.Views;
using RoutesPlanning.Android.Utils;
using RoutesPlanning.Core.ViewModels.Destinations;

namespace RoutesPlanning.Android.Views.Destinations
{
    [Activity(Label = "RoutesPlanning", Theme = "@style/RoutesPlanningLightTheme")]
    public class DestinationsActivity : MvxActivity<DestinationsViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_destinations);
        }

        public override bool DispatchTouchEvent(MotionEvent ev)
        {
            var res = base.DispatchTouchEvent(ev);
            KeyboardHelper.HideKeyboardIfTouchOutsideEditText(this, ev);
            return res;
        }
    }
}
