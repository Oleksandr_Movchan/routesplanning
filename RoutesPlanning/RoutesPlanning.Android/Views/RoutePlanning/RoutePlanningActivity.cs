using Android.App;
using Android.Content.Res;
using Android.OS;
using Android.Views;
using Android.Widget;
using MvvmCross.DroidX.RecyclerView;
using MvvmCross.Platforms.Android.Views;
using RoutesPlanning.Core.ViewModels.RoutePlanning;

namespace RoutesPlanning.Android.Views.RoutePlanning
{
    [Activity(Theme = "@style/RoutesPlanningLightTheme")]
    public class RoutePlanningActivity : MvxActivity<RoutePlanningViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_route_planning);

            var recyclerView = FindViewById<MvxRecyclerView>(Resource.Id.route_planning_recycler_view);
            var layoutManager = new MvxGuardedLinearLayoutManager(this);
            recyclerView.SetLayoutManager(layoutManager);

            var activityIndicatorView = FindViewById<ProgressBar>(Resource.Id.route_planning_indicator_view);
            activityIndicatorView.IndeterminateTintList = ColorStateList.ValueOf(Theme.GetPrimaryColor());
        }

        protected override void OnStart()
        {
            base.OnStart();

            this.AdjustToolbar(Resource.Id.route_planning_toolbar);
            Title = GetString(Resource.String.routes_planning_title);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == global::Android.Resource.Id.Home)
            {
                HandleBackNavigation();
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }

        private void HandleBackNavigation()
        {
            ViewModel.BackCommand.Execute();
        }
    }
}
