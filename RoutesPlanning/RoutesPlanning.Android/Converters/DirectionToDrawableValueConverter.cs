using System;
using System.Globalization;
using MvvmCross.Converters;

namespace RoutesPlanning.Android.Converters
{
    public class DirectionToDrawableValueConverter : MvxValueConverter<string, int>
    {
        protected override int Convert(string value, Type targetType, object parameter, CultureInfo culture)
        {
            return value switch
            {
                "turn-right" => Resource.Drawable.ic_right_24,
                "turn-left" => Resource.Drawable.ic_left_24,
                _ => Resource.Drawable.ic_no_direction_24
            };
        }
    }
}
