using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MvvmCross.IoC;
using MvvmCross.Platforms.Android.Core;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.Plugin;
using MvvmCross.ViewModels;
using RoutesPlanning.Android.Converters;
using RoutesPlanning.Android.Infrastructure.Dialogs;
using RoutesPlanning.Android.Infrastructure.Http;
using RoutesPlanning.Core;
using RoutesPlanning.Infrastructure.Dialogs;
using RoutesPlanning.Infrastructure.Http;

namespace RoutesPlanning.Android
{
    public class Setup : MvxAndroidSetup<App>
    {
        protected override IMvxIoCProvider InitializeIoC()
        {
            var ioc = base.InitializeIoC();

            ioc.LazyConstructAndRegisterSingleton<INativeHttpClientHandlerFactory, NativeHttpClientHandlerFactory>();
            ioc.LazyConstructAndRegisterSingleton<IDialogsService, DialogsService>();

            return ioc;
        }

        protected override IEnumerable<Assembly> ValueConverterAssemblies
        {
            get
            {
                var toReturn = base.ValueConverterAssemblies.ToList();

                toReturn.Add(typeof(BooleanToVisibilityValueConverter).Assembly);
                toReturn.Add(typeof(DirectionToDrawableValueConverter).Assembly);

                return toReturn;
            }
        }

        protected override void InitializeApp(IMvxPluginManager pluginManager, IMvxApplication app)
        {
            base.InitializeApp(pluginManager, app);

            Plugin.CurrentActivity.CrossCurrentActivity.Current.Init(MvxAndroidApplication.Instance);
        }
    }
}
