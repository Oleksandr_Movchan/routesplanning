using Android.App;
using Android.Runtime;
using Android.Widget;
using AndroidX.RecyclerView.Widget;
using MvvmCross.DroidX.RecyclerView;

namespace RoutesPlanning.Android
{
    [Preserve(AllMembers = true)]
    public class LinkerPleaseInclude
    {
        public void Include(RecyclerView.ViewHolder vh, MvxRecyclerView list)
        {
            vh.ItemView.Click += (sender, args) => { };
            vh.ItemView.LongClick += (sender, args) => { };
            list.ItemsSource = null;
            list.Click += (sender, args) => { };
            list.SetLayoutManager(new MvxGuardedLinearLayoutManager(new Activity()));
        }

        public void Include(EditText et)
        {
            et.TextChanged += (s, args) => { };
            et.BeforeTextChanged += (s, args) => { };
            et.AfterTextChanged += (s, args) => { };
        }

        public void Include(TextView tv)
        {
            tv.TextChanged += (s, args) => { };
        }

        public void Include(MvxGuardedLinearLayoutManager llm)
        {
            llm.Orientation = MvxGuardedLinearLayoutManager.Vertical;
        }
    }
}
