using Android.App;
using Android.Views;
using Android.Views.InputMethods;

namespace RoutesPlanning.Android.Utils
{
    public class KeyboardHelper
    {
        public static bool HideKeyboardIfTouchOutsideEditText(Activity activity, MotionEvent ev)
        {
            var result = false;

            if (ev.Action == MotionEventActions.Up || ev.Action == MotionEventActions.Down || ev.Action == MotionEventActions.Move)
            {
                var focusedItem = activity.CurrentFocus;

                switch (focusedItem)
                {
                    case global::Android.Widget.EditText _:
                    {
                        var rect = new global::Android.Graphics.Rect();
                        focusedItem.GetGlobalVisibleRect(rect);
                        if (!rect.Contains((int)ev.RawX, (int)ev.RawY))
                        {
                            var inputMethodManager = (InputMethodManager)activity.GetSystemService(global::Android.Content.Context.InputMethodService);
                            inputMethodManager.HideSoftInputFromWindow(focusedItem.WindowToken, 0);
                            focusedItem.ClearFocus();
                            result = true;
                        }

                        break;
                    }
                }
            }

            return result;
        }
    }
}
